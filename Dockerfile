FROM php

RUN cp /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini
RUN sed -i 's/memory_limit = 128M/memory_limit = 256M/g' /usr/local/etc/php/php.ini

RUN mkdir /usr/src/app
WORKDIR /usr/src/app
COPY . ./

ENTRYPOINT ["php", "eloq.php"]
