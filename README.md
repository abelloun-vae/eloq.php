# ENGLISH

# Eloq.php

Eloq compiler to php as a target language. ( The reason for choosing php is that I used Eloq in the context of web programming on my customers' LAMP architecture ).


## Description

Eloq is a programming language that sits somewhere between Lisp, Haskell and Yacc. The goal is to unify into a single formalism the three languages I previously developed with a strong attention given to variables scope management :
- Sense / Expression : declarative and purely fonctionnal language for general purpose computation
- Dialectic / Grammar : declarative language to express parsing expression grammar parsers ( with arbitrary left recursion and operator precedence )
- SeTML / Markup : Markup language for IHM description in general and GUI in particular.

Until now, only the strict minimum for complete self-hosting of the compiler has been developed. Compared to older versions, it still lacks the following :
- Markup : some code is already present but not yet integrated into the compiler,
- Type-classes generation (Functor, Foldable, ...) is not implemented yet,
- floating point numbers
- ...

Even though the language follows a strictly functionnal paradigm, effects are available in order to manage input-output, they are accessible through abuse of the target language language and due to the fact that the compiler is mainly an isomorphic translation. 
Indeedn without an optimizing compiler, effects management is pretty much classical and intuitive, but it will be necessary to find a relevant mecanism in the future, hopefully without resorting to monads...

Syntax of the language is largely inspired by that of haskell and javascript.


## Source code organisation

- `/bootstraps/bootstrap` : all the source dumped in a single file
- `/bootstraps/bootstrap.php` : php bootstrap
- `/src/App/Eloq/Eloq` : Tying the knot. This module integrate the semantics for each language into a single one.
- `/src/App/Eloq/Eloq.Parser` : Tying the knot. This module integrate the grammars for each language into a single one.
- `/src/libs/Dialects` : Modules for each dialect.
- `/src/libs/Dialects/Expression` : functionnal general purpose programming language
- `/src/libs/Dialects/Grammar` : declarative language for parser description
- `/src/libs/Dialects/Markup` : markup language ( work in progress )
- `/src/libs/Data` : fonctions to work on base types ( Array, Integer, String, .... )
- `/src/libs/Graph` : Functionnal implementation of Tarjan algorithm for cycles detection
- `/src/libs/Generics` : Usual type classes implementation expressed as fonctions of two lower level functions (toList et unCurry ( if you have a better name ), they both can be expressed as an even lower function I call uncurryG )
- `/src/libs/*` : other fonctions ...









# FRENCH

# Eloq.php

Implementation du compilateur Eloq dans le langage cible php.

## Description


Eloq est un langage que je situerais quelque part entre Lisp, Haskell et Yacc. Le but est d'unifier au sein d'un seul formalisme les trois langages que j'ai précédemment développés, avec une attention particulière portée à la gestion du scope entre les langages :
- Sense / Expression : un langage déclaratif et fonctionnel pour la computation générale
- Dialectic / Grammar : un langage de description de parsers
- SeTML / Markup : un langage de balises pour la description d'IHM en général et de GUI en particulier

Jusqu'à présent, uniquement le strict nécessaire pour un self-hosting complet du compilateur a été développé. Par rapport aux anciennes versions il manque les fonctionnalités suivantes :
- Markup : certaine brique du langage de balise sont présentes dans les sources, mais pas encore intégrées dans le compilateur
- la génération des type-classes (Functor, Foldable) n'est pas encore implémentée
- les virgules flottantes
- ...

Même si le langage suit un paradigme purement fonctionnel, des effets sont disponibles afin de gérer les entrées-sorties, ils sont fournis par le langage cible.
En l'absence d'optimisations du compilateur, la gestion des effets reste classique et intuitive, en revanche, il sera nécessaire de trouver un mécanisme pour leur gestion correcte en présence des optimisations.
L'idéal serait d'échapper aux monades.

La syntaxe est largement inspirée de celles d'Haskell et de javascript ...

## Organisation

- `/bootstraps/bootstrap` : intégralité du code source dumpé dans un seul fichier
- `/bootstraps/bootstrap.php` : bootstrap en php
- `/src/App/Eloq/Eloq` : Module qui "attache" la sémantique des dialectes dans un tout cohérent
- `/src/App/Eloq/Eloq.Parser` : Grammaire qui "attache" la syntaxe des dialectes dans un tout cohérent
- `/src/libs/Dialects` : les modules définissant chacun des 3 dialectes.
- `/src/libs/Dialects/Expression` : le module pour le langage de programmation fonctionnel
- `/src/libs/Dialects/Grammar` : le module pour le langage de définition de parser
- `/src/libs/Dialects/Markup` : le module pour le langage de balises
- `/src/libs/Data` : fonctions pour type de données de base ( Array, Integer, String, .... )
- `/src/libs/Graph` : algorithme de tarjan pour les cycles
- `/src/libs/Generics` : implémentation des type-classes "usuelles" en fonction de deux fonctions de base (toList et unCurry ( faute d'un meilleur nom ) )
- `/src/libs/*` : autres fonctions ...



