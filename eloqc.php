#!/use/local/bin/php
<?php
namespace Eloq;
chdir(__DIR__);
// bootstrap test
$boot = __DIR__ . '/bootstraps/bootstrap.php';
$inputFile = $argv[1];
echo '<?php (function() {' . (include($boot))()->nspace['Eloq']['compileFile']($inputFile) . '})()();';