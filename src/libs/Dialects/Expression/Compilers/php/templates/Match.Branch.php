// START BRANCH <?php
(function ($VALUE_TO_MATCH){mkUse $ Rho.addE (Rho.rem (Rho.add b.expr.free $ b.gard == () ? Rho.zer : b.gard.$.free) $ Names.addParams Rho.zer b.patt) {"#{data.self}", Var data.self}} {
	//BRANCH PATTERN
	if ( !{indents 1 $ compilePattern b.patt "$VALUE_TO_MATCH"} ) return;
	//BRANCH GUARD
	{M.from b.gard "" \h -> "if ( !({h.code}) ) return;"}
	// BRANCH EXP
	return [{indents 1 b.expr.code}];
})
// END BRANCH
