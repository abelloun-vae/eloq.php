// BEGIN SCO <?php
((function ($f){mkUse freeIn} {
	// NULL INIT
	{L.implode ";\n\t" $ L.filter (\x -> x) $ L.fmap InitIn ds};
	// VALUES INIT
	{L.implode ";\n\t" $ L.filter (\x -> x) $ L.fmap (indents 1 . compileIn) ds};
	return $f(
		{L.implode ",\n\t\t" $ L.filter (\e -> e) $ L.fmap compileOut fds}
	);
})(function (
	{mkParams scopeOut}
){mkUse $ Rho.rem fe.free scopeOut} {
	return {e};
}))
