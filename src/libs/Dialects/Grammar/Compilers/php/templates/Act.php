// START ACTION <?php
$node = (function ($oldState){mkUse $ Sig.addL fa.free ("state", "context")} {
	{indents 1 a}
	if ($node)
		$node = {b ? "" : "["}{indents 1 e}(
			self::$expressionContext[0],
			array_merge(self::$expressionContext[1], [
				'context' => array_merge($context, ['state' => $state, 'oldState' => $oldState]),
				'this' => $node[0],
			])
		){b ? "" : "]"};
	return $node;
})($state);
// END ACTION
