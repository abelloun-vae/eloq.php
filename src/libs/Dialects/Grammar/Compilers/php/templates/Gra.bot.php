// <?php
// START SCO_BOT
return ((new class ($expressionContext, uniqid('parser_'), {A.implode ", " $ A.fmap (\v -> "${v}") free}) {

	static $expressionContext;
	function __construct(
		public $expressionContextDyn,
		public $parserId,
		{A.implode ",\n\t\t" $ A.fmap (\v -> "public ${v}") free}
	) {
		self::$expressionContext = $expressionContextDyn;
	}

	function __invoke($rule) {
		return $this->parse(function ($context, &$state, $parser) use($rule) {
			return self::\{'__rule__' . $rule}($context, $state, $parser);
		});
	}

	function parse ($token, &$uid = 0) {
		return function ($dataInput) use ($token) {
			$memo = [];
			$max = 0;
			$context = [
				'memo' => &$memo,
				'data' => array_merge(['dir' => '', 'file' => '', 'fileStack' => []], $dataInput[0]),
			];
			$state = [
				'input' => $dataInput[1],
				'precedence' => ['>', 0],
				'ignore' => 0,
				'pos' => 0,
				'line' => 1,
				'col' => 1,
				'max' => &$max,
				'uid' => &$uid,
			];
			try {
				$node = $token($context, $state, $this);
			} catch (Throwable $e) {
				throw new Exception($e->getMessage() . sprintf(
					"\n\nForwarded Parse Error in %1\$s on max line %2\$s:%3\$s :\n%4\$s%5\$s",
					!empty($dataInput[0]['file']) ? "\"\{$dataInput[0]['file']}\"" : "string",
					substr_count($dataInput[1], "\n", 0, $state['max']) + 1,
					$state['max'] + 1 - strrpos(substr("\n" . $dataInput[1], 0, $state['max'] + 1), "\n"),
					substr($state['input'], $state['max'] - $state['pos'], 255),
					!empty($dataInput[0]['fileStack']) ? "\n###fileStack : " . implode(",\n", array_reverse($dataInput[0]['fileStack'])) : ''
				), 0, $e);
			}
			if (!$node)
				throw new Exception(sprintf(
					"Parse Error in %1\$s on max line %2\$s:%3\$s :\n%4\$s%5\$s",
					!empty($dataInput[0]['file']) ? "\"\{$dataInput[0]['file']}\"" : "string",
					substr_count($dataInput[1], "\n", 0, $state['max']) + 1,
					$state['max'] + 1 - strrpos(substr("\n" . $dataInput[1], 0, $state['max'] + 1), "\n"),
					substr($state['input'], $state['max'] - $state['pos'], 255),
					!empty($dataInput[0]['fileStack']) ? "\n###fileStack : " . implode(",\n", array_reverse($dataInput[0]['fileStack'])) : ''
				));
			if ( $state['input'] )
				throw new Exception(sprintf(
					"Parse error in %1\$s on max line %2\$s:%3\$s :\n%4\$s\n\nUnexpected end of input on line %5\$s :\n%6\$s%7\$s",
					!empty($dataInput[0]['file']) ? "\"\{$dataInput[0]['file']}\"" : "string",
					substr_count($dataInput[1], "\n", 0, $state['max']) + 1,
					$state['max'] + 1 - strrpos(substr("\n" . $dataInput[1], 0, $state['max'] + 1), "\n"),
					substr($state['input'], $state['max'] - $state['pos'], 255),
					substr_count($dataInput[1], "\n", 0, $state['pos']) + 1,
					substr($state['input'], 0, 255),
					!empty($dataInput[0]['fileStack']) ? "\n###fileStack : " . implode(",\n", array_reverse($dataInput[0]['fileStack'])) : ''
				));
			return $node[0];
		};
	}

	static function pass ($context, &$state, $parser) {
		{ignore ? (#tpl("Memo.pass.php")) : ""}
		{ignore ? "return $passed;" : "return false;"}
	}

	{indents 1 body}

}));
// END SCO_BOT
