// START MEMO_IGN <?php
$IGNKEY = "\{$state['pos']}{$parser->parserId}IGN";
if ( !empty($context['memo'][$IGNKEY]) ) {
	$mem = $context['memo'][$IGNKEY];
	if ( $mem[1] ) {
		$state['input'] = $mem[0]['input'];
		$state['pos'] = $mem[0]['pos'];
		$state['line'] = $mem[0]['line'];
		$state['col'] = $mem[0]['col'];
	}
	$passed = $mem[1];
} else {
	$node = false;
	$passed = false;
	$state['ignore'] = 1;
	do {
		{indents 2 ignore}
		$passed = $passed || $node;
	} while ($node);
	$state['ignore'] = 0;
	$context['memo'][$IGNKEY] = [$state, $passed];
}
// END MEMO_IGN
