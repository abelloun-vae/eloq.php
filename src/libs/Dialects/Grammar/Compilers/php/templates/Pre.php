// START PRE_ASS <?php
if (
	{n} < $state['precedence'][1] ||
	{n} === $state['precedence'][1] &&
	$state['precedence'][0] === '<'
) $node = false;
else {
	$oldprecss[] = $state['precedence'];
	$state['precedence'] = ['{d}', {n}];
	{indents 1 body}
	$state['precedence'] = array_pop($oldprecss);
}
// END PRE_ASS
