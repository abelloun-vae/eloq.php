//START QTF* <?php
$nodes[] = [];
do {
	{indents 1 body}
	if ($node) $nodes[count($nodes) - 1][] = $node[0];
} while ($node);
$node = [array_reduce(
	array_reverse(array_pop($nodes)),
	function($a, $e) {return self::$expressionContext[0]::cons($e, $a);},
	self::$expressionContext[0]::$nil
)];
//END QTF*
