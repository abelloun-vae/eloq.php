// START REP <?php
$node = false;
{fl.code}
if ($node) {
	$node = $parser->parse(function ($context, &$state){mkUse $ Sig.remL fr.free ('context', 'state')} {
		{indents 2 fr.code}
		return $node;
	}, $state['uid'])($node[0]);
	$node = [$node];
}
// END REP
