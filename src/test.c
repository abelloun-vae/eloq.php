#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//######################
// Ids
//######################
typedef enum NodeId NodeId;
typedef enum DataId DataId;
typedef enum TypeId TypeId;
typedef enum NameId NameId;

//######################
// Node
//######################
typedef struct Node S_Node;
typedef S_Node* Node;
struct Node {
	NodeId id;
};

//######################
// NodeId
//######################
enum NodeId {
	NodeId_Boolean,
	NodeId_Integer,
	NodeId_Char,
	NodeId_String,
	NodeId_Tuple,
	NodeId_Array,
	NodeId_Record,
	NodeId_HashMap,
	NodeId_Data,
	NodeId_Type,
	NodeId_Name,
	NodeId_Closure,
};

//######################
// DataId
//######################
enum DataId {
	DataId_Nil_0,
	DataId_Cons_2,
};

//######################
// TypeId
//######################
enum TypeId {
	TypeId_,
};

//######################
// NameId
//######################
enum NameId {
	NameId_,
};

//######################
// Types
//######################
typedef struct Node_Boolean S_Node_Boolean;
typedef S_Node_Boolean* Node_Boolean;

typedef struct Node_Integer S_Node_Integer;
typedef S_Node_Integer* Node_Integer;

typedef struct Node_Char S_Node_Char;
typedef S_Node_Char* Node_Char;

typedef struct Node_String S_Node_String;
typedef S_Node_String* Node_String;

typedef struct Node_Tuple S_Node_Tuple;
typedef S_Node_Tuple* Node_Tuple;

typedef struct Node_Array S_Node_Array;
typedef S_Node_Array* Node_Array;

typedef struct Node_Record S_Node_Record;
typedef S_Node_Record* Node_Record;

typedef struct Node_HashMap S_Node_HashMap;
typedef S_Node_HashMap* Node_HashMap;

typedef struct Node_Data S_Node_Data;
typedef S_Node_Data* Node_Data;

typedef struct Node_Type S_Node_Type;
typedef S_Node_Type* Node_Type;

typedef struct Node_Name S_Node_Name;
typedef S_Node_Name* Node_Name;

typedef struct Node_Closure S_Node_Closure;
typedef S_Node_Closure* Node_Closure;


//######################
// Structs
//######################
struct Node_Boolean {
	NodeId id;
	int b;
};

struct Node_Integer {
	NodeId id;
	int i;
};

struct Node_Char {
	NodeId id;
	char c[4];
	int size;
};

struct Node_String {
	NodeId id;
	char* s;
	int len;
};

struct Node_Tuple {
	NodeId id;
	int size;
	Node* data;
};

struct Node_Array {
	NodeId id;
	int size;
	Node* data;
};

struct Node_Record {
	NodeId id;
	int size;
	Node* data;
};

struct Node_HashMap {
	NodeId id;
	int size;
	Node* data;
};

struct Node_Data {
	NodeId id;
	DataId name;
	int arity;
	int size;
	Node* data;
};

struct Node_Type {
	NodeId id;
	TypeId name;
	int arity;
	int size;
	Node* data;
};

struct Node_Name {
	NodeId id;
	NameId name;
	int arity;
	int size;
	Node* data;
};

struct Node_Closure {
	NodeId id;
	int size;
	Node* scope;
	Node (*lambda)(Node* sco, Node arg);
};


//######################
// Prototypes
//######################

int				toHash_Node				(int seed, int size, Node n);
int				toBoolean_Node			(Node n);
int				reflexivity_Node			(Node l, Node r);
void				print_Node				(int indent, Node n);
void				free_Node					(Node n);
Node			apply_Node				(Node l, Node r);
Node			access_Node				(Node l, Node r);

//######################
// prototypes Boolean
//######################
int				toHash_Node_Boolean		(int seed, int size, Node_Boolean n);
int				toBoolean_Node_Boolean		(Node_Boolean n);
int				reflexivity_Node_Boolean		(Node_Boolean l, Node_Boolean r);
void				print_Node_Boolean			(int indent, Node_Boolean n);
void				free_Node_Boolean			(Node_Boolean n);
Node_Boolean		alloc_Node_Boolean			();
Node_Boolean		init_Node_Boolean			(Node_Boolean n, int b);
Node			make_Node_Boolean		(int b);
Node			apply_Node_Boolean		(Node_Boolean l, Node r);
Node			access_Node_Boolean		(Node_Boolean l, Node r);

//######################
// prototypes Integer
//######################
int				toHash_Node_Integer		(int seed, int size, Node_Integer n);
int				toBoolean_Node_Integer		(Node_Integer n);
int				reflexivity_Node_Integer		(Node_Integer l, Node_Integer r);
void				print_Node_Integer			(int indent, Node_Integer n);
void				free_Node_Integer			(Node_Integer n);
Node_Integer		alloc_Node_Integer			();
Node_Integer		init_Node_Integer			(Node_Integer n, int i);
Node			make_Node_Integer		(int i);
Node			apply_Node_Integer		(Node_Integer l, Node r);
Node			access_Node_Integer		(Node_Integer l, Node r);

//######################
// prototypes Char
//######################
int				toHash_Node_Char		(int seed, int size, Node_Char n);
int				toBoolean_Node_Char		(Node_Char n);
int				reflexivity_Node_Char		(Node_Char l, Node_Char r);
void				print_Node_Char			(int indent, Node_Char n);
void				free_Node_Char			(Node_Char n);
Node_Char		alloc_Node_Char			();
Node_Char		init_Node_Char			(Node_Char n, char c[4], int size);
Node			make_Node_Char		(char c[4], int size);
Node			apply_Node_Char		(Node_Char l, Node r);
Node			access_Node_Char		(Node_Char l, Node r);

//######################
// prototypes String
//######################
int				toHash_Node_String		(int seed, int size, Node_String n);
int				toBoolean_Node_String		(Node_String n);
int				reflexivity_Node_String		(Node_String l, Node_String r);
void				print_Node_String			(int indent, Node_String n);
void				free_Node_String			(Node_String n);
Node_String		alloc_Node_String			(char* s, int len);
Node_String		init_Node_String			(Node_String n, char* s, int len);
Node			make_Node_String		(char* s, int len);
Node			apply_Node_String		(Node_String l, Node r);
Node			access_Node_String		(Node_String l, Node r);

//######################
// prototypes Tuple
//######################
int				toHash_Node_Tuple		(int seed, int size, Node_Tuple n);
int				toBoolean_Node_Tuple		(Node_Tuple n);
int				reflexivity_Node_Tuple		(Node_Tuple l, Node_Tuple r);
void				print_Node_Tuple			(int indent, Node_Tuple n);
void				free_Node_Tuple			(Node_Tuple n);
Node_Tuple		alloc_Node_Tuple			(int size, Node* data);
Node_Tuple		init_Node_Tuple			(Node_Tuple n, int size, Node* data);
Node			make_Node_Tuple		(int size, Node* data);
Node			apply_Node_Tuple		(Node_Tuple l, Node r);
Node			access_Node_Tuple		(Node_Tuple l, Node r);

//######################
// prototypes Array
//######################
int				toHash_Node_Array		(int seed, int size, Node_Array n);
int				toBoolean_Node_Array		(Node_Array n);
int				reflexivity_Node_Array		(Node_Array l, Node_Array r);
void				print_Node_Array			(int indent, Node_Array n);
void				free_Node_Array			(Node_Array n);
Node_Array		alloc_Node_Array			(int size, Node* data);
Node_Array		init_Node_Array			(Node_Array n, int size, Node* data);
Node			make_Node_Array		(int size, Node* data);
Node			apply_Node_Array		(Node_Array l, Node r);
Node			access_Node_Array		(Node_Array l, Node r);

//######################
// prototypes Record
//######################
int				toHash_Node_Record		(int seed, int size, Node_Record n);
int				toBoolean_Node_Record		(Node_Record n);
int				reflexivity_Node_Record		(Node_Record l, Node_Record r);
void				print_Node_Record			(int indent, Node_Record n);
void				free_Node_Record			(Node_Record n);
Node_Record		alloc_Node_Record			(int size, Node* data);
Node_Record		init_Node_Record			(Node_Record n, int size, Node* data);
Node			make_Node_Record		(int size, Node* data);
Node			apply_Node_Record		(Node_Record l, Node r);
Node			access_Node_Record		(Node_Record l, Node r);

//######################
// prototypes HashMap
//######################
int				toHash_Node_HashMap		(int seed, int size, Node_HashMap n);
int				toBoolean_Node_HashMap		(Node_HashMap n);
int				reflexivity_Node_HashMap		(Node_HashMap l, Node_HashMap r);
void				print_Node_HashMap			(int indent, Node_HashMap n);
void				free_Node_HashMap			(Node_HashMap n);
Node_HashMap		alloc_Node_HashMap			(int size, Node* data);
Node_HashMap		init_Node_HashMap			(Node_HashMap n, int size, Node* data);
Node			make_Node_HashMap		(int size, Node* data);
Node			apply_Node_HashMap		(Node_HashMap l, Node r);
Node			access_Node_HashMap		(Node_HashMap l, Node r);

//######################
// prototypes Data
//######################
int				toHash_Node_Data		(int seed, int size, Node_Data n);
int				toBoolean_Node_Data		(Node_Data n);
int				reflexivity_Node_Data		(Node_Data l, Node_Data r);
void				print_Node_Data			(int indent, Node_Data n);
void				free_Node_Data			(Node_Data n);
Node_Data		alloc_Node_Data			(DataId name, int arity, int size, Node* data);
Node_Data		init_Node_Data			(Node_Data n, DataId name, int arity, int size, Node* data);
Node			make_Node_Data		(DataId name, int arity, int size, Node* data);
Node			apply_Node_Data		(Node_Data l, Node r);
Node			access_Node_Data		(Node_Data l, Node r);

//######################
// prototypes Type
//######################
int				toHash_Node_Type		(int seed, int size, Node_Type n);
int				toBoolean_Node_Type		(Node_Type n);
int				reflexivity_Node_Type		(Node_Type l, Node_Type r);
void				print_Node_Type			(int indent, Node_Type n);
void				free_Node_Type			(Node_Type n);
Node_Type		alloc_Node_Type			(TypeId name, int arity, int size, Node* data);
Node_Type		init_Node_Type			(Node_Type n, TypeId name, int arity, int size, Node* data);
Node			make_Node_Type		(TypeId name, int arity, int size, Node* data);
Node			apply_Node_Type		(Node_Type l, Node r);
Node			access_Node_Type		(Node_Type l, Node r);

//######################
// prototypes Name
//######################
int				toHash_Node_Name		(int seed, int size, Node_Name n);
int				toBoolean_Node_Name		(Node_Name n);
int				reflexivity_Node_Name		(Node_Name l, Node_Name r);
void				print_Node_Name			(int indent, Node_Name n);
void				free_Node_Name			(Node_Name n);
Node_Name		alloc_Node_Name			(NameId name, int arity, int size, Node* data);
Node_Name		init_Node_Name			(Node_Name n, NameId name, int arity, int size, Node* data);
Node			make_Node_Name		(NameId name, int arity, int size, Node* data);
Node			apply_Node_Name		(Node_Name l, Node r);
Node			access_Node_Name		(Node_Name l, Node r);

//######################
// prototypes Closure
//######################
int				toHash_Node_Closure		(int seed, int size, Node_Closure n);
int				toBoolean_Node_Closure		(Node_Closure n);
int				reflexivity_Node_Closure		(Node_Closure l, Node_Closure r);
void				print_Node_Closure			(int indent, Node_Closure n);
void				free_Node_Closure			(Node_Closure n);
Node_Closure		alloc_Node_Closure			(int size, Node* scope, Node (*lambda)(Node* sco, Node arg));
Node_Closure		init_Node_Closure			(Node_Closure n, int size, Node* scope, Node (*lambda)(Node* sco, Node arg));
Node			make_Node_Closure		(int size, Node* scope, Node (*lambda)(Node* sco, Node arg));
Node			apply_Node_Closure		(Node_Closure l, Node r);
Node			access_Node_Closure		(Node_Closure l, Node r);


//######################
// Type Prototypes
//######################

Node			internal_add				(Node l, Node r);
Node			internal_dif				(Node l, Node r);
Node			internal_div				(Node l, Node r);
Node			internal_mul				(Node l, Node r);
Node			internal_pow				(Node l, Node r);
Node			internal_divc				(Node l, Node r);
Node			internal_divr				(Node l, Node r);
Node			internal_divf				(Node l, Node r);
Node			internal_mod				(Node l, Node r);
Node			internal_xwing				(Node l, Node r);

void				Char_print				(char* c, int size);
int				Char_getSize				(char* c);

Node			injectString				(char* s, int len);
Node			getNodeName				(Node n);
Node			internal_concat				(Node l, Node r);





Node			getDataName				(Node_Data n);

Node			getTypeName				(Node_Type n);

Node			getNameName				(Node_Name n);


//######################
// Helpers
//######################
void print_indent(int n) {
	for(int i = 0; i < n; i++)
		printf("\t");
}

//######################
// Type Implementation
//######################
//######################
// Base Boolean
//######################
void free_Node_Boolean(Node_Boolean n) {
	free(n);
}

Node_Boolean alloc_Node_Boolean() {
	return malloc(sizeof(S_Node_Boolean) + 0);
}

Node_Boolean init_Node_Boolean(Node_Boolean this, int b) {
	this->id = NodeId_Boolean;
	this->b = b > 0 ? 1 : 0;
	return this;
}

Node make_Node_Boolean(int b) {
	return (Node) init_Node_Boolean(alloc_Node_Boolean(), b);
}

//######################
// Base Integer
//######################
void free_Node_Integer(Node_Integer n) {
	free(n);
}

Node_Integer alloc_Node_Integer() {
	return malloc(sizeof(S_Node_Integer) + 0);
}

Node_Integer init_Node_Integer(Node_Integer this, int i) {
	this->id = NodeId_Integer;
	this->i = i;
	return this;
}

Node make_Node_Integer(int i) {
	return (Node) init_Node_Integer(alloc_Node_Integer(), i);
}

//######################
// Base Char
//######################
void free_Node_Char(Node_Char n) {
	free(n);
}

Node_Char alloc_Node_Char() {
	return malloc(sizeof(S_Node_Char) + 0);
}

Node_Char init_Node_Char(Node_Char this, char c[4], int size) {
	this->id = NodeId_Char;
	for(int i = 0; i < 4; i++) this->c[i] = i < size ? c[i] : 0;
	this->size = size;
	return this;
}

Node make_Node_Char(char c[4], int size) {
	return (Node) init_Node_Char(alloc_Node_Char(), c, size);
}

//######################
// Base String
//######################
void free_Node_String(Node_String n) {
	free(n);
}

Node_String alloc_Node_String(char* s, int len) {
	return malloc(sizeof(S_Node_String) + (s ? 0 : (sizeof(char) * 4 * len)));
}

Node_String init_Node_String(Node_String this, char* s, int len) {
	this->id = NodeId_String;
	this->s = s ? s : ((char*)(this + 1));
	this->len = len;
	return this;
}

Node make_Node_String(char* s, int len) {
	return (Node) init_Node_String(alloc_Node_String(s, len), s, len);
}

//######################
// Base Tuple
//######################
void free_Node_Tuple(Node_Tuple n) {
	free(n);
}

Node_Tuple alloc_Node_Tuple(int size, Node* data) {
	return malloc(sizeof(S_Node_Tuple) + sizeof(Node) * size);
}

Node_Tuple init_Node_Tuple(Node_Tuple this, int size, Node* data) {
	this->id = NodeId_Tuple;
	this->size = size;
	this->data = (Node*) (this + 1); if(data) for(int i = 0; i < size; i++) this->data[i] = data[i];
	return this;
}

Node make_Node_Tuple(int size, Node* data) {
	return (Node) init_Node_Tuple(alloc_Node_Tuple(size, data), size, data);
}

//######################
// Base Array
//######################
void free_Node_Array(Node_Array n) {
	free(n);
}

Node_Array alloc_Node_Array(int size, Node* data) {
	return malloc(sizeof(S_Node_Array) + sizeof(Node) * size);
}

Node_Array init_Node_Array(Node_Array this, int size, Node* data) {
	this->id = NodeId_Array;
	this->size = size;
	this->data = (Node*) (this + 1); if(data) for(int i = 0; i < size; i++) this->data[i] = data[i];
	return this;
}

Node make_Node_Array(int size, Node* data) {
	return (Node) init_Node_Array(alloc_Node_Array(size, data), size, data);
}

//######################
// Base Record
//######################
void free_Node_Record(Node_Record n) {
	free(n);
}

Node_Record alloc_Node_Record(int size, Node* data) {
	return malloc(sizeof(S_Node_Record) + HashMap_size(size));
}

Node_Record init_Node_Record(Node_Record this, int size, Node* data) {
	this->id = NodeId_Record;
	this->size = size;
	this->data = (HashMap) (this + 1); if (data) HashMap_init(this->data, size, data)
	return this;
}

Node make_Node_Record(int size, Node* data) {
	return (Node) init_Node_Record(alloc_Node_Record(size, data), size, data);
}

//######################
// Base HashMap
//######################
void free_Node_HashMap(Node_HashMap n) {
	free(n);
}

Node_HashMap alloc_Node_HashMap(int size, Node* data) {
	return malloc(sizeof(S_Node_HashMap) + HashMap_size(size));
}

Node_HashMap init_Node_HashMap(Node_HashMap this, int size, Node* data) {
	this->id = NodeId_HashMap;
	this->size = size;
	this->data = (HashMap) (this + 1); if (data) HashMap_init(this->data, size, data)
	return this;
}

Node make_Node_HashMap(int size, Node* data) {
	return (Node) init_Node_HashMap(alloc_Node_HashMap(size, data), size, data);
}

//######################
// Base Data
//######################
void free_Node_Data(Node_Data n) {
	free(n);
}

Node_Data alloc_Node_Data(DataId name, int arity, int size, Node* data) {
	return malloc(sizeof(S_Node_Data) + sizeof(Node) * size);
}

Node_Data init_Node_Data(Node_Data this, DataId name, int arity, int size, Node* data) {
	this->id = NodeId_Data;
	this->name = name;
	this->arity = arity;
	this->size = size;
	this->data = (Node*) (this + 1); if(data) for(int i = 0; i < size; i++) this->data[i] = data[i];
	return this;
}

Node make_Node_Data(DataId name, int arity, int size, Node* data) {
	return (Node) init_Node_Data(alloc_Node_Data(name, arity, size, data), name, arity, size, data);
}

//######################
// Base Type
//######################
void free_Node_Type(Node_Type n) {
	free(n);
}

Node_Type alloc_Node_Type(TypeId name, int arity, int size, Node* data) {
	return malloc(sizeof(S_Node_Type) + sizeof(Node) * size);
}

Node_Type init_Node_Type(Node_Type this, TypeId name, int arity, int size, Node* data) {
	this->id = NodeId_Type;
	this->name = name;
	this->arity = arity;
	this->size = size;
	this->data = (Node*) (this + 1); if(data) for(int i = 0; i < size; i++) this->data[i] = data[i];
	return this;
}

Node make_Node_Type(TypeId name, int arity, int size, Node* data) {
	return (Node) init_Node_Type(alloc_Node_Type(name, arity, size, data), name, arity, size, data);
}

//######################
// Base Name
//######################
void free_Node_Name(Node_Name n) {
	free(n);
}

Node_Name alloc_Node_Name(NameId name, int arity, int size, Node* data) {
	return malloc(sizeof(S_Node_Name) + sizeof(Node) * size);
}

Node_Name init_Node_Name(Node_Name this, NameId name, int arity, int size, Node* data) {
	this->id = NodeId_Name;
	this->name = name;
	this->arity = arity;
	this->size = size;
	this->data = (Node*) (this + 1); if(data) for(int i = 0; i < size; i++) this->data[i] = data[i];
	return this;
}

Node make_Node_Name(NameId name, int arity, int size, Node* data) {
	return (Node) init_Node_Name(alloc_Node_Name(name, arity, size, data), name, arity, size, data);
}

//######################
// Base Closure
//######################
void free_Node_Closure(Node_Closure n) {
	free(n);
}

Node_Closure alloc_Node_Closure(int size, Node* scope, Node (*lambda)(Node* sco, Node arg)) {
	return malloc(sizeof(S_Node_Closure) + sizeof(Node) * size);
}

Node_Closure init_Node_Closure(Node_Closure this, int size, Node* scope, Node (*lambda)(Node* sco, Node arg)) {
	this->id = NodeId_Closure;
	this->size = size;
	this->scope = (Node*) (this + 1); if(scope) for(int i = 0; i < size; i++) this->scope[i] = scope[i];
	this->lambda = lambda;
	return this;
}

Node make_Node_Closure(int size, Node* scope, Node (*lambda)(Node* sco, Node arg)) {
	return (Node) init_Node_Closure(alloc_Node_Closure(size, scope, lambda), size, scope, lambda);
}


void print_Node_Boolean(int indent, Node_Boolean n) {
	if (n->b) printf("true");
	else printf("false");
}

int reflexivity_Node_Boolean(Node_Boolean l, Node_Boolean r) {
	return l->b == r->b;
}

int toBoolean_Node_Boolean(Node_Boolean n) {
	return n->b;
}


void print_Node_Integer(int indent, Node_Integer n) {
	printf("%i", n->i);
}

int reflexivity_Node_Integer(Node_Integer l, Node_Integer r) {
	return l->i == r->i;
}

int toBoolean_Node_Integer(Node_Integer n) {
	return n->i;
}







Node internal_add(Node l, Node r) {
	return make_Node_Integer(((Node_Integer)l)->i + ((Node_Integer)r)->i);
}

Node internal_dif(Node l, Node r) {
	return make_Node_Integer(((Node_Integer)l)->i - ((Node_Integer)r)->i);
}

Node internal_div(Node l, Node r) {
	return make_Node_Integer(((Node_Integer)l)->i / ((Node_Integer)r)->i);
}

Node internal_mul(Node l, Node r) {
	return make_Node_Integer(((Node_Integer)l)->i * ((Node_Integer)r)->i);
}

Node internal_pow(Node l, Node r) {
	return make_Node_Integer(pow(((Node_Integer)l)->i, ((Node_Integer)r)->i));
}

Node internal_divc(Node l, Node r) {
	double x = (double) ((Node_Integer)l)->i;
	double y = (double) ((Node_Integer)r)->i;
	return make_Node_Integer((x + y - 1) / y);
}

Node internal_divr(Node l, Node r) {
	return make_Node_Integer(((Node_Integer)l)->i / ((Node_Integer)r)->i);
}

Node internal_divf(Node l, Node r) {
	return make_Node_Integer(((Node_Integer)l)->i / ((Node_Integer)r)->i);
}

Node internal_mod(Node l, Node r) {
	return make_Node_Integer(((Node_Integer)l)->i % ((Node_Integer)r)->i);
}

Node internal_xwing(Node l, Node r) {
	int x = ((Node_Integer) l)->i;
	int y = ((Node_Integer) r)->i;
	return make_Node_Integer(x > y ? 1 : (x < y ? -1 : 0));
}



void print_Node_Char(int indent, Node_Char n) {
	Char_print(n->c, n->size);
}

int reflexivity_Node_Char(Node_Char l, Node_Char r) {
	if (l->size != r->size)
		return 0;
	for(int i = 0; i < l->size; i++)
		if (l->c[i] != r->c[i])
			return 0;
	return 1;
}

int toBoolean_Node_Char(Node_Char n) {
	return 1;
}

//######################
// Low-Level
//######################
void Char_print(char* c, int size) {
	for(int i = 0; i < size; i++)
		printf("%c", c[i]);
}

int Char_getSize(char* c) {
	if ( c[0] == "�"[0] || c[0] == "�"[0] )
		return 2;
	return 1;
}


void print_Node_String(int indent, Node_String n) {
	for(int i = 0; i < n->len; i++)
		Char_print(n->s + i * 4, Char_getSize(n->s + i * 4));
}

int reflexivity_Node_String(Node_String l, Node_String r) {
	if (l->len != r->len)
		return 0;
	for(int i = 0; i < l->len; i++)
		for(int j = 0; j < Char_getSize(l->s + i * 4); j++)
			if (l->s[i * 4 + j] != r->s[i * 4 + j])
				return 0;
	return 1;
}

int toBoolean_Node_String(Node_String n) {
	return n->len;
}



Node injectString(char* s, int len) {
	Node_String ns = (Node_String) make_Node_String(0, len);
	int delta = 0;
	int csize;
	for(int i = 0; i < len; i++) {
		csize = Char_getSize(s + i + delta);
		for(int j = 0; j < 4; j++)
			ns->s[i * 4 + j] = j < csize ? s[i + j + delta] : 0;
		delta += csize - 1;
	}
	return (Node) ns;
}



struct hashmap *map = hashmap_new(sizeof(struct user), 0, 0, 0,
                                     user_hash, user_compare, NULL);



Node getNodeName(Node n) {
	switch (n->id) {
		case NodeId_Boolean: return injectString("Boolean", 7);
		case NodeId_Integer: return injectString("Integer", 7);
		case NodeId_Char: return injectString("Char", 4);
		case NodeId_String: return injectString("String", 6);
		case NodeId_Data: return injectString("Data", 4);
		default: return injectString("UNKNOWN", 7);
	}
}

Node internal_concat(Node l, Node r) {
	Node_String s = (Node_String) make_Node_String(0, ((Node_String)l)->len + ((Node_String)r)->len);
	for(int i = 0; i < ((Node_String)l)->len * 4; i++)
		s->s[i] = ((Node_String)l)->s[i];
	for(int i = 0; i < ((Node_String)r)->len * 4; i++)
		s->s[((Node_String)l)->len * 4 + i] = ((Node_String)r)->s[i];
	return (Node) s;
}


void print_Node_Array(int indent, Node_Array n) {
	printf("[");
	if (n->size == 1) {
		print_Node(indent, n->data[0]);
	} else if (n->size > 1) {
		for (int i = 0; i < n->size; i++) {
			printf("\n\t");
			print_indent(indent);
			print_Node(indent + 1, (n->data[i]));
			//~ if (i < n->size - 1)
				//~ printf(",");
		}
		printf("\n");
		print_indent(indent);
	}
	printf("]");
}

int reflexivity_Node_Array(Node_Array l, Node_Array r) {
	if (l->size != r->size)
		return 0;
	for(int i = 0; i < l->size; i++)
		if (!reflexivity(l->data[i], r->data[i]))
			return 0;
	return 1;
}

int toBoolean_Node_Array(Node_Array n) {
	return n->size;
}

Node access_Node_Array(Node_Array l, Node r) {
	return l->data[((Node_Integer)r)->i];
}


void print_Node_Tuple(int indent, Node_Tuple n) {
	printf("{");
	if (n->size == 1) {
		print_Node(indent, n->data[0]);
	} else if (n->size > 1) {
		for (int i = 0; i < n->size; i++) {
			printf("\n\t");
			print_indent(indent);
			print_Node(indent + 1, (n->data[i]));
			//~ if (i < n->size - 1)
				//~ printf(",");
		}
		printf("\n");
		print_indent(indent);
	}
	printf("}");
}

int reflexivity_Node_Tuple(Node_Tuple l, Node_Tuple r) {
	if (l->size != r->size)
		return 0;
	for(int i = 0; i < l->size; i++)
		if (!reflexivity(l->data[i], r->data[i]))
			return 0;
	return 1;
}

int toBoolean_Node_Tuple(Node_Tuple n) {
	return n->size;
}

Node access_Node_Tuple(Node_Tuple l, Node r) {
	return l->data[((Node_Integer)r)->i];
}

void print_Node_Data(int indent, Node_Data n) {
	if (n->size) printf("(");
	print_Node(indent, getDataName(n));
	for (int i = 0; i < n->size; i++) {
		printf(" ");
		print_Node(indent, (n->data[i]));
	}
	if (n->size) printf(")");
}

int reflexivity_Node_Data(Node_Data l, Node_Data r) {
	if (l->name != r->name || l->arity != r->arity || l->size != r->size)
		return 0;
	for(int i = 0; i < l->size; i++)
		if (!reflexivity(l->data[i], r->data[i]))
			return 0;
	return 1;
}

int toBoolean_Node_Data(Node_Data n) {
	switch(n->name) {
		case DataId_Nil_0: return 0;
		default: return 1;
	}
}


Node apply_Node_Data(Node_Data f, Node a) {
	if (f->arity == 0) {
		printf("Arity exceeded\n");
		exit(1);
	}
	Node nodes[f->size + 1];
	for(int i = 0; i < f->size; i++)
		nodes[i] = f->data[i];
	nodes[f->size] = a;
	return make_Node_Data(f->name, f->arity, f->size + 1, nodes);
}

Node getDataName(Node_Data n) {
	switch (n->name) {
		case DataId_Nil_0: return injectString("Nil", 3);
		case DataId_Cons_2: return injectString("Cons", 4);
		default: return injectString("UNKNOWN", 7);
	}
}


void print_Node_Type(int indent, Node_Type n) {
	if (n->size) printf("(");
	print_Node(indent, getTypeName(n));
	for (int i = 0; i < n->size; i++) {
		printf(" ");
		print_Node(indent, (n->data[i]));
	}
	if (n->size) printf(")");
}

int reflexivity_Node_Type(Node_Type l, Node_Type r) {
	if (l->name != r->name || l->arity != r->arity || l->size != r->size)
		return 0;
	for(int i = 0; i < l->size; i++)
		if (!reflexivity(l->data[i], r->data[i]))
			return 0;
	return 1;
}

int toBoolean_Node_Type(Node_Type n) {
	return 1;
}


Node apply_Node_Type(Node_Type f, Node a) {
	if (f->arity == 0) {
		printf("Arity exceeded\n");
		exit(1);
	}
	Node nodes[f->size + 1];
	for(int i = 0; i < f->size; i++)
		nodes[i] = f->data[i];
	nodes[f->size] = a;
	return make_Node_Type(f->name, f->arity, f->size + 1, nodes);
}


Node getTypeName(Node_Type n) {
	switch (n->name) {
		default: return injectString("UNKNOWN", 7);
	}
}


void print_Node_Name(int indent, Node_Name n) {
	if (n->size) printf("(");
	print_Node(indent, getNameName(n));
	for (int i = 0; i < n->size; i++) {
		printf(" ");
		print_Node(indent, (n->data[i]));
	}
	if (n->size) printf(")");
}

int reflexivity_Node_Name(Node_Name l, Node_Name r) {
	if (l->name != r->name || l->arity != r->arity || l->size != r->size)
		return 0;
	for(int i = 0; i < l->size; i++)
		if (!reflexivity(l->data[i], r->data[i]))
			return 0;
	return 1;
}

int toBoolean_Node_Name(Node_Name n) {
	return 1;
}


Node apply_Node_Name(Node_Name f, Node a) {
	if (f->arity == 0) {
		printf("Arity exceeded\n");
		exit(1);
	}
	Node nodes[f->size + 1];
	for(int i = 0; i < f->size; i++)
		nodes[i] = f->data[i];
	nodes[f->size] = a;
	return make_Node_Name(f->name, f->arity, f->size + 1, nodes);
}

Node getNameName(Node_Name n) {
	switch (n->name) {
		default: return injectString("UNKNOWN", 7);
	}
}



void print_Node_Closure(int indent, Node_Closure n) {
	printf("<CLOSURE>");
}

int reflexivity_Node_Closure(Node_Closure l, Node_Closure r) {
	return 0;
}

int toBoolean_Node_Closure(Node_Closure n) {
	return 1;
}






Node apply_Node_Closure(Node_Closure f, Node a) {
	return f->lambda(f->scope, a);
}








//######################
//
//######################
Node refl(Node l, Node r) {
	return make_Node_Boolean(reflexivity(l, r));
}

Node toBool(Node n) {
	return make_Node_Boolean(toBoolean(n));
}

Node coaAnd(Node l, Node r) {
	return toBoolean(l) ? r : l;
}

Node coaOr(Node l, Node r) {
	return toBoolean(l) ? l : r;
}

Node compose(Node* scope, Node arg) {
	return apply(scope[0], apply(scope[1], arg));
}


Node _lambda__aro__dot_0_dot_0_dot_1_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0(Node* sco, Node _variable_x) {

	return apply(_variable_x, _variable_x);
}

Node _lambda__aro__dot_0_dot_0_dot_1_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_1_dot_0_dot_0_dot_0_dot_0_dot_1_dot_0(Node* sco, Node _variable_v) {
	Node _variable_x = sco[0];
	return apply(apply(_variable_x, _variable_x), _variable_v);
}

Node _lambda__aro__dot_0_dot_0_dot_1_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_1_dot_0_dot_0(Node* sco, Node _variable_x) {
	Node _variable_f = sco[0];
	return apply(_variable_f, make_Node_Closure(1, (Node[]) {_variable_x}, &_lambda__aro__dot_0_dot_0_dot_1_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_1_dot_0_dot_0_dot_0_dot_0_dot_1_dot_0));
}

Node _lambda__aro__dot_0_dot_0_dot_1_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0(Node* sco, Node _variable_f) {

	return apply(make_Node_Closure(0, (Node[]) {}, &_lambda__aro__dot_0_dot_0_dot_1_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0), make_Node_Closure(1, (Node[]) {_variable_f}, &_lambda__aro__dot_0_dot_0_dot_1_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_1_dot_0_dot_0));
}

Node _lambda__aro__dot_0_dot_0_dot_1_dot_0_dot_0_dot_0_dot_1_dot_0_dot_0_dot_0_dot_0(Node* sco, Node _variable_n) {
	Node _variable_fact = sco[0];
	return ((Node_Boolean) (refl(_variable_n, make_Node_Integer(0))))->b ? make_Node_Integer(1) : internal_mul(_variable_n, apply(_variable_fact, internal_dif(_variable_n, make_Node_Integer(1))));
}

Node _lambda__aro__dot_0_dot_0_dot_1_dot_0_dot_0_dot_0_dot_1_dot_0_dot_0(Node* sco, Node _variable_fact) {

	return make_Node_Closure(1, (Node[]) {_variable_fact}, &_lambda__aro__dot_0_dot_0_dot_1_dot_0_dot_0_dot_0_dot_1_dot_0_dot_0_dot_0_dot_0);
}

//######################
// MAIN
//######################
int main(int argc, char** argv) {
	Node _variable_id = make_Node_Closure(0, 0, &_id);
	Node expression = make_Node_Tuple(19, (Node[]) {
		make_Node_Record(1, (Node[]) {{injectString("e", 1), make_Node_Integer(1)}}),
		apply(apply(make_Node_Closure(0, (Node[]) {}, &_lambda__aro__dot_0_dot_0_dot_1_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0), make_Node_Closure(0, (Node[]) {}, &_lambda__aro__dot_0_dot_0_dot_1_dot_0_dot_0_dot_0_dot_1_dot_0_dot_0)), make_Node_Integer(5)),
		make_Node_Boolean(0),
		injectString("Hello", 5),
		make_Node_Integer(1),
		internal_add(make_Node_Integer(1), make_Node_Integer(1)),
		make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
			internal_add(make_Node_Integer(1), internal_mul(make_Node_Integer(2), make_Node_Integer(3))),
			make_Node_Data(DataId_Nil_0, 0, 0, 0)
		}),
		access(make_Node_Array(3, (Node[]) {
			internal_div(internal_dif(make_Node_Integer(8), make_Node_Integer(2)), make_Node_Integer(3)),
			make_Node_Integer(2),
			make_Node_Integer(3)
		}), make_Node_Integer(0)),
		access(make_Node_Tuple(2, (Node[]) {
			internal_pow(make_Node_Integer(2), make_Node_Integer(8)),
			make_Node_Integer(10)
		}), make_Node_Integer(1)),
		make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
			make_Node_Integer(1),
			make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
				make_Node_Integer(2),
				make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
					make_Node_Integer(3),
					make_Node_Data(DataId_Nil_0, 0, 0, 0)
				})
			})
		}),
		(((Node_Data) make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
			make_Node_Integer(1),
			make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
				make_Node_Integer(2),
				make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
					make_Node_Integer(3),
					make_Node_Data(DataId_Nil_0, 0, 0, 0)
				})
			})
		}))->data[0]),
		(((Node_Data) make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
			make_Node_Integer(1),
			make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
				make_Node_Integer(2),
				make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
					make_Node_Integer(3),
					make_Node_Data(DataId_Nil_0, 0, 0, 0)
				})
			})
		}))->data[1]),
		make_Node_Array(3, (Node[]) {
			make_Node_Integer(1),
			make_Node_Integer(2),
			make_Node_Integer(3)
		}),
		coaAnd(refl(make_Node_Integer(1), make_Node_Integer(1)), injectString("oui", 3)),
		coaOr(make_Node_Integer(0), injectString("non", 3)),
		refl(make_Node_Data(DataId_Nil_0, 0, 0, 0), make_Node_Data(DataId_Nil_0, 0, 0, 0)),
		refl(make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
			make_Node_Integer(1),
			make_Node_Data(DataId_Nil_0, 0, 0, 0)
		}), make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
			make_Node_Integer(1),
			make_Node_Data(DataId_Nil_0, 0, 0, 0)
		})),
		refl(make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
			make_Node_Integer(1),
			make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
				make_Node_Integer(2),
				make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
					make_Node_Integer(3),
					make_Node_Data(DataId_Nil_0, 0, 0, 0)
				})
			})
		}), make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
			make_Node_Integer(1),
			make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
				make_Node_Integer(2),
				make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
					make_Node_Integer(3),
					make_Node_Data(DataId_Nil_0, 0, 0, 0)
				})
			})
		})),
		refl(make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
			make_Node_Integer(1),
			make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
				make_Node_Integer(2),
				make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
					make_Node_Integer(3),
					make_Node_Data(DataId_Nil_0, 0, 0, 0)
				})
			})
		}), make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
			make_Node_Integer(1),
			make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
				make_Node_Integer(2),
				make_Node_Data(DataId_Cons_2, 0, 2, (Node[]) {
					make_Node_Integer(4),
					make_Node_Data(DataId_Nil_0, 0, 0, 0)
				})
			})
		}))
	});
	print_Node(0, expression);
	printf("\n");
	return 0;
}
