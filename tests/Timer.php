<?php

namespace Eloq;

class Timer {

	static function getTime() {
		return hrtime(true);
	}

	private $start = 0, $delta = 0, $stack = [], $format;

	function __construct($format = null, $start = null) {
		$this->start = $start ?: static::getTime();
		$this->delta = $this->start;
		$this->format = $format ?: function($x){return $x;};
	}

	function getTotal($mul = 1) {
		return ($this->format)((static::getTime() - $this->start) * $mul);
	}

	function getDelta($mul = 1) {
		$delta = $this->delta;
		$this->delta = static::getTime();
		return ($this->format)(($this->delta - $delta) * $mul);
	}

	function push() {
		$this->stack[] = $this->delta;
		return $this;
	}

	function pop() {
		$this->delta = array_pop($this->stack);
		return $this;
	}

}
