<?php

namespace Eloq;

class TimerFormat {

	static private $units = ['fs' => 1000, 'ps' => 1000, 'ns' => 1000, 'µs' => 1000, 'ms' => 1000, 's' => 60, 'm' => 60, 'h' => 24, 'd' => 30, 'm' => 12, 'y' => 365, 'c' => 100];

	static function ns($time, $unit) {
		$mul = self::$units[array_keys(self::$units)[array_search($unit, array_keys(self::$units)) - 1]];
		$sep = array_merge([$unit => '.'], ['m' => ':', 'h' => ':'])[$unit];
		return [
			'a' => intval($time * $mul),
			'ua' => array_keys(self::$units)[array_search($unit, array_keys(self::$units)) - 1],
			'u' => $unit,
			't' => intval($time * 1000) / 1000,
			'z' => intval($time) . $sep . 		substr("000" . intval(($time  - intval($time)) * $mul), 0 - strlen("" . ($mul - 1)))
		];
	}

	static function focus($time, $unit) {
		$mul = array_values(self::$units)[$unit];
		return ($mul && $time > $mul) ?
			self::focus($time / $mul, $unit + 1) :
			self::ns($time, array_keys(self::$units)[$unit]);
	}

	static function format($format, $ns) {
		return preg_replace_callback(
			'/\{([a-zA-Z_][a-zA-Z_0-9]*)\}/',
			function ($p) use ($ns){ return $ns[$p[1]];},
			$format
		);
	}



	private $format = "";

	function __construct($format) {
		$this->format = $format;
	}

	function __invoke($time, $unit = 2) {
		return self::format($this->format, self::focus($time, $unit));
	}

}
