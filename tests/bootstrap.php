#!/use/local/bin/php
<?php
namespace Eloq;
include('Timer.php');
include('TimerFormat.php');

chdir(__DIR__);
// bootstrap test
$boot = __DIR__ . '/../bootstraps/bootstrap.php';
$index = __DIR__ . "/../" . $argv[1];
$out = "/tmp/bootstrap.php";
//####################################################################################
if ( file_exists($out) ) unlink($out);
$timer = new Timer(new TimerFormat("{z}{u}"));
$ns = (include($boot))()->nspace;
echo "BOOT : ", $timer->getDelta(), "\n";
$auto = 0;
for ($i = 0; $i < 5; $i++) {
	//####################################################################################
	$bcode = '<?php ' . $ns['Eloq']['compileFile']($index);
	echo "PASS $i : ", $timer->getDelta(), "\n";
	//####################################################
	if (file_exists($out) && file_get_contents($out) === $bcode)
		$auto++;
	else $auto = 0;
	file_put_contents($out, $bcode);
	$ns = (include($out))()->nspace;
	echo "BOOT $i : ", $timer->getDelta(), "\n";
	//####################################################
	if ( $auto ) {
		echo "OK\n";
		exit(0);
	}
}
exit(1);
