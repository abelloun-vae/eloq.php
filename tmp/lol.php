<?php (function() {// \header body -> <?php
return function ($_scope = []) {

	return (new class($_scope) {

		private function lambda($_scope) {
			$self = $this;
			$_variable_die = $_scope['die'];
			$_variable_implementation = $_scope['implementation'];
			return // BEGIN SCO <?php
			((function ($f) use (
				&$_variable_implementation,
				$self
			) {
				// NULL INIT
				$_variable_constants = null;
				$_variable_parser = null;
				$_variable_results = null;
				// VALUES INIT
				$_variable_constants = [
					"pi" => (314159 / 100000),
					"e" => (271828 / 100000)
				];
				$_variable_parser = ((function($expressionContext) {
					// <?php
					$node = (function ($expressionContext) {
						// SCO TOP
						// <?php
						// START SCO_BOT
						return ((new class ($expressionContext, uniqid('parser_'), ) {
						
							static $expressionContext;
							function __construct(
								public $expressionContextDyn,
								public $parserId,
								
							) {
								self::$expressionContext = $expressionContextDyn;
							}
						
							function __invoke($rule) {
								return $this->parse(function ($context, &$state, $parser) use($rule) {
									return self::{'__rule__' . $rule}($context, $state, $parser);
								});
							}
						
							function parse ($token, &$uid = 0) {
								return function ($dataInput) use ($token) {
									$memo = [];
									$max = 0;
									$context = [
										'memo' => &$memo,
										'data' => array_merge(['dir' => '', 'file' => '', 'fileStack' => []], $dataInput[0]),
									];
									$state = [
										'input' => $dataInput[1],
										'precedence' => ['>', 0],
										'ignore' => 0,
										'pos' => 0,
										'line' => 1,
										'col' => 1,
										'max' => &$max,
										'uid' => &$uid,
									];
									try {
										$node = $token($context, $state, $this);
									} catch (Throwable $e) {
										throw new Exception($e->getMessage() . sprintf(
											"\n\nForwarded Parse Error in %1\$s on max line %2\$s:%3\$s :\n%4\$s%5\$s",
											!empty($dataInput[0]['file']) ? "\"{$dataInput[0]['file']}\"" : "string",
											substr_count($dataInput[1], "\n", 0, $state['max']) + 1,
											$state['max'] + 1 - strrpos(substr("\n" . $dataInput[1], 0, $state['max'] + 1), "\n"),
											substr($state['input'], $state['max'] - $state['pos'], 255),
											!empty($dataInput[0]['fileStack']) ? "\n###fileStack : " . implode(",\n", array_reverse($dataInput[0]['fileStack'])) : ''
										), 0, $e);
									}
									if (!$node)
										throw new Exception(sprintf(
											"Parse Error in %1\$s on max line %2\$s:%3\$s :\n%4\$s%5\$s",
											!empty($dataInput[0]['file']) ? "\"{$dataInput[0]['file']}\"" : "string",
											substr_count($dataInput[1], "\n", 0, $state['max']) + 1,
											$state['max'] + 1 - strrpos(substr("\n" . $dataInput[1], 0, $state['max'] + 1), "\n"),
											substr($state['input'], $state['max'] - $state['pos'], 255),
											!empty($dataInput[0]['fileStack']) ? "\n###fileStack : " . implode(",\n", array_reverse($dataInput[0]['fileStack'])) : ''
										));
									if ( $state['input'] )
										throw new Exception(sprintf(
											"Parse error in %1\$s on max line %2\$s:%3\$s :\n%4\$s\n\nUnexpected end of input on line %5\$s :\n%6\$s%7\$s",
											!empty($dataInput[0]['file']) ? "\"{$dataInput[0]['file']}\"" : "string",
											substr_count($dataInput[1], "\n", 0, $state['max']) + 1,
											$state['max'] + 1 - strrpos(substr("\n" . $dataInput[1], 0, $state['max'] + 1), "\n"),
											substr($state['input'], $state['max'] - $state['pos'], 255),
											substr_count($dataInput[1], "\n", 0, $state['pos']) + 1,
											substr($state['input'], 0, 255),
											!empty($dataInput[0]['fileStack']) ? "\n###fileStack : " . implode(",\n", array_reverse($dataInput[0]['fileStack'])) : ''
										));
									return $node[0];
								};
							}
						
							static function pass ($context, &$state, $parser) {
								// START MEMO_IGN <?php
						$IGNKEY = "{$state['pos']}{$parser->parserId}IGN";
						if ( !empty($context['memo'][$IGNKEY]) ) {
							$mem = $context['memo'][$IGNKEY];
							if ( $mem[1] ) {
								$state['input'] = $mem[0]['input'];
								$state['pos'] = $mem[0]['pos'];
								$state['line'] = $mem[0]['line'];
								$state['col'] = $mem[0]['col'];
							}
							$passed = $mem[1];
						} else {
							$node = false;
							$passed = false;
							$state['ignore'] = 1;
							do {
								// <?php // START REG
								$node = false;
								if ( preg_match("/^\\s/", $state['input'], $m) ) {
									$len = strlen($m[0]);
									$node = [$m];
									$state['input'] = substr($state['input'], $len);
									$state['pos'] += $len;
									$state['line'] += substr_count($m[0], "\n");
									$x = strrpos($m[0], "\n");
									$state['col'] = $x === false ? $state['col'] + $len : ($len - $x);
									$state['max'] = max($state['max'], $state['pos']);
								} elseif ( !$state['ignore'] ) {
									$savestate = $state;
									if ($parser::pass($context, $state, $parser)) {
										if ( preg_match("/^\\s/", $state['input'], $m) ) {
											$len = strlen($m[0]);
											$node = [$m];
											$state['input'] = substr($state['input'], $len);
											$state['pos'] += $len;
											$state['line'] += substr_count($m[0], "\n");
											$x = strrpos($m[0], "\n");
											$state['col'] = $x === false ? $state['col'] + $len : ($len - $x);
											$state['max'] = max($state['max'], $state['pos']);
										} else {
											$state = $savestate;
										}
									}
								}
								//END REG
								
									if (!$node) {
										$node = false;
									}
								$passed = $passed || $node;
							} while ($node);
							$state['ignore'] = 0;
							$context['memo'][$IGNKEY] = [$state, $passed];
						}
						// END MEMO_IGN
						
								return $passed;
							}
						
							static function __rule__spaces ($context, &$state, $parser) {// <?php // START RULE
								// START MEMO <?php
								$ruleMemoKey = "{$state['pos']}{$state['precedence'][0]}{$state['precedence'][1]}\$rule:spaces.\${$parser->parserId}";
								if ( !empty($context['memo'][$ruleMemoKey]) ) {
									$mem = $context['memo'][$ruleMemoKey];
									if ( $mem[1] ) {
										$state['input'] = $mem[0]['input'];
										$state['pos'] = $mem[0]['pos'];
										$state['line'] = $mem[0]['line'];
										$state['col'] = $mem[0]['col'];
										$state['precedence'] = $mem[0]['precedence'];
									}
									$node = $mem[1];
								} else {
									// START MEMO_BODY
									
									$nodes = [];
									$node = false;
									// <?php // START REG
									$node = false;
									if ( preg_match("/^\\s/", $state['input'], $m) ) {
										$len = strlen($m[0]);
										$node = [$m];
										$state['input'] = substr($state['input'], $len);
										$state['pos'] += $len;
										$state['line'] += substr_count($m[0], "\n");
										$x = strrpos($m[0], "\n");
										$state['col'] = $x === false ? $state['col'] + $len : ($len - $x);
										$state['max'] = max($state['max'], $state['pos']);
									} elseif ( !$state['ignore'] ) {
										$savestate = $state;
										if ($parser::pass($context, $state, $parser)) {
											if ( preg_match("/^\\s/", $state['input'], $m) ) {
												$len = strlen($m[0]);
												$node = [$m];
												$state['input'] = substr($state['input'], $len);
												$state['pos'] += $len;
												$state['line'] += substr_count($m[0], "\n");
												$x = strrpos($m[0], "\n");
												$state['col'] = $x === false ? $state['col'] + $len : ($len - $x);
												$state['max'] = max($state['max'], $state['pos']);
											} else {
												$state = $savestate;
											}
										}
									}
									//END REG
									
									
									// END MEMO_BODY
									$context['memo'][$ruleMemoKey] = [$state, $node];
								}
								// END MEMO
								
								return $node;
								// END RULE
								
								;}
							static function __rule__S ($context, &$state, $parser) {// <?php // START RULE
								// START MEMO <?php
								$ruleMemoKey = "{$state['pos']}{$state['precedence'][0]}{$state['precedence'][1]}\$rule:S.\${$parser->parserId}";
								if ( !empty($context['memo'][$ruleMemoKey]) ) {
									$mem = $context['memo'][$ruleMemoKey];
									if ( $mem[1] ) {
										$state['input'] = $mem[0]['input'];
										$state['pos'] = $mem[0]['pos'];
										$state['line'] = $mem[0]['line'];
										$state['col'] = $mem[0]['col'];
										$state['precedence'] = $mem[0]['precedence'];
									}
									$node = $mem[1];
								} else {
									// START MEMO_BODY
									
									$nodes = [];
									$node = false;
									// VAR_RULE <?php
									$node = $parser::__rule__expr($context, $state, $parser);
									
									
									
									// END MEMO_BODY
									$context['memo'][$ruleMemoKey] = [$state, $node];
								}
								// END MEMO
								
								return $node;
								// END RULE
								
								;}
							static function __rule__expr ($context, &$state, $parser) {// <?php // START RULE
								
								$nodes = [];
								$node = false;
								// START MEMO <?php
								$growMemoKey = "{$state['pos']}{$state['precedence'][0]}{$state['precedence'][1]}\$grow:expr.\${$parser->parserId}";
								if ( !empty($context['memo'][$growMemoKey]) ) {
									$mem = $context['memo'][$growMemoKey];
									if ( $mem[1] ) {
										$state['input'] = $mem[0]['input'];
										$state['pos'] = $mem[0]['pos'];
										$state['line'] = $mem[0]['line'];
										$state['col'] = $mem[0]['col'];
										$state['precedence'] = $mem[0]['precedence'];
									}
									$node = $mem[1];
								} else {
									// START MEMO_BODY
									// START ALT_GRO <?php
										$start = $state;
										// START ALT_GRO SEED
										// VAR_RULE <?php
										$node = $parser::__rule__const($context, $state, $parser);
										
										
											if (!$node) {
												// VAR_RULE <?php
										$node = $parser::__rule__int($context, $state, $parser);
										
										
											if (!$node) {
												$node = false;
											}
											}
										// END ALT_GRO SEED
										// START ALT_GRO LOOP
										if ($node) do {
											$context['memo'][$growMemoKey] = [$state, $node];
											$pos = $state['pos'];
											$state = $start;
											// START ALT_GRO GROW
											// START ACTION <?php
											$node = (function ($oldState) use (&$state, &$parser, &$context) {
												// <?php // START SEQ
													$backstatess[] = $state;
													$nodess[] = [];
													
													// VAR_RULE <?php
												$node = $parser::__rule__expr($context, $state, $parser);
												
												
													if (!$node) {
														$state = array_pop($backstatess);
														array_pop($nodess);
													} else {
														$nodess[count($nodess) - 1][] = $node[0];
														
													// START PRE_ASS <?php
												if (
													5 < $state['precedence'][1] ||
													5 === $state['precedence'][1] &&
													$state['precedence'][0] === '<'
												) $node = false;
												else {
													$oldprecss[] = $state['precedence'];
													$state['precedence'] = ['>', 5];
													// START SEL <?php
													// <?php // START SEQ
														$backstatess[] = $state;
														$nodess[] = [];
														
														// <?php // START STR
													$node = false;
													if ( ( substr($state['input'], 0, 1) === "^" ) ) {
													
														$node = ["^"];
														$state['input'] = substr($state['input'], 1);
														$state['pos'] += 1;
														$state['line'] += 0;
														$x = strrpos("^", "\n");
														$state['col'] = $x === false ? $state['col'] + 1 : (1 - $x);
														$state['max'] = max($state['max'], $state['pos']);
													} elseif ( !$state['ignore'] ) {
														$savestate = $state;
														if ($parser::pass($context, $state, $parser)) {
															if ( ( substr($state['input'], 0, 1) === "^" ) ) {
													
																$node = ["^"];
																$state['input'] = substr($state['input'], 1);
																$state['pos'] += 1;
																$state['line'] += 0;
																$x = strrpos("^", "\n");
																$state['col'] = $x === false ? $state['col'] + 1 : (1 - $x);
																$state['max'] = max($state['max'], $state['pos']);
															} else {
																$state = $savestate;
															}
														}
													}
													//END STR
													
														if (!$node) {
															$state = array_pop($backstatess);
															array_pop($nodess);
														} else {
															$nodess[count($nodess) - 1][] = $node[0];
															
														// VAR_RULE <?php
													$node = $parser::__rule__expr($context, $state, $parser);
													
													
														if (!$node) {
															$state = array_pop($backstatess);
															array_pop($nodess);
														} else {
															$nodess[count($nodess) - 1][] = $node[0];
															array_pop($backstatess); $node = [array_pop($nodess)];
														}
														
														}
														
													// END SEQ
													
													if ($node)
														$node = [$node[0]['1']];
													// END SEL
													
													$state['precedence'] = array_pop($oldprecss);
												}
												// END PRE_ASS
												
													if (!$node) {
														$state = array_pop($backstatess);
														array_pop($nodess);
													} else {
														$nodess[count($nodess) - 1][] = $node[0];
														array_pop($backstatess); $node = [array_pop($nodess)];
													}
													
													}
													
												// END SEQ
												
												if ($node)
													$node = [// \header body -> <?php
												(function($self, $_scope){
													$_variable_this = $_scope['this'];
													return ((($_variable_this)[0]) ** (($_variable_this)[1]));
												})(
														self::$expressionContext[0],
														array_merge(self::$expressionContext[1], [
															'context' => array_merge($context, ['state' => $state, 'oldState' => $oldState]),
															'this' => $node[0],
														])
													)];
												return $node;
											})($state);
											// END ACTION
											
												if (!$node) {
													// START ACTION <?php
											$node = (function ($oldState) use (&$state, &$parser, &$context) {
												// <?php // START SEQ
													$backstatess[] = $state;
													$nodess[] = [];
													
													// VAR_RULE <?php
												$node = $parser::__rule__expr($context, $state, $parser);
												
												
													if (!$node) {
														$state = array_pop($backstatess);
														array_pop($nodess);
													} else {
														$nodess[count($nodess) - 1][] = $node[0];
														
													// START PRE_ASS <?php
												if (
													4 < $state['precedence'][1] ||
													4 === $state['precedence'][1] &&
													$state['precedence'][0] === '<'
												) $node = false;
												else {
													$oldprecss[] = $state['precedence'];
													$state['precedence'] = ['<', 4];
													// START SEL <?php
													// <?php // START SEQ
														$backstatess[] = $state;
														$nodess[] = [];
														
														// <?php // START STR
													$node = false;
													if ( ( substr($state['input'], 0, 1) === "*" ) ) {
													
														$node = ["*"];
														$state['input'] = substr($state['input'], 1);
														$state['pos'] += 1;
														$state['line'] += 0;
														$x = strrpos("*", "\n");
														$state['col'] = $x === false ? $state['col'] + 1 : (1 - $x);
														$state['max'] = max($state['max'], $state['pos']);
													} elseif ( !$state['ignore'] ) {
														$savestate = $state;
														if ($parser::pass($context, $state, $parser)) {
															if ( ( substr($state['input'], 0, 1) === "*" ) ) {
													
																$node = ["*"];
																$state['input'] = substr($state['input'], 1);
																$state['pos'] += 1;
																$state['line'] += 0;
																$x = strrpos("*", "\n");
																$state['col'] = $x === false ? $state['col'] + 1 : (1 - $x);
																$state['max'] = max($state['max'], $state['pos']);
															} else {
																$state = $savestate;
															}
														}
													}
													//END STR
													
														if (!$node) {
															$state = array_pop($backstatess);
															array_pop($nodess);
														} else {
															$nodess[count($nodess) - 1][] = $node[0];
															
														// VAR_RULE <?php
													$node = $parser::__rule__expr($context, $state, $parser);
													
													
														if (!$node) {
															$state = array_pop($backstatess);
															array_pop($nodess);
														} else {
															$nodess[count($nodess) - 1][] = $node[0];
															array_pop($backstatess); $node = [array_pop($nodess)];
														}
														
														}
														
													// END SEQ
													
													if ($node)
														$node = [$node[0]['1']];
													// END SEL
													
													$state['precedence'] = array_pop($oldprecss);
												}
												// END PRE_ASS
												
													if (!$node) {
														$state = array_pop($backstatess);
														array_pop($nodess);
													} else {
														$nodess[count($nodess) - 1][] = $node[0];
														array_pop($backstatess); $node = [array_pop($nodess)];
													}
													
													}
													
												// END SEQ
												
												if ($node)
													$node = [// \header body -> <?php
												(function($self, $_scope){
													$_variable_this = $_scope['this'];
													return ((($_variable_this)[0]) * (($_variable_this)[1]));
												})(
														self::$expressionContext[0],
														array_merge(self::$expressionContext[1], [
															'context' => array_merge($context, ['state' => $state, 'oldState' => $oldState]),
															'this' => $node[0],
														])
													)];
												return $node;
											})($state);
											// END ACTION
											
												if (!$node) {
													// START ACTION <?php
											$node = (function ($oldState) use (&$state, &$parser, &$context) {
												// <?php // START SEQ
													$backstatess[] = $state;
													$nodess[] = [];
													
													// VAR_RULE <?php
												$node = $parser::__rule__expr($context, $state, $parser);
												
												
													if (!$node) {
														$state = array_pop($backstatess);
														array_pop($nodess);
													} else {
														$nodess[count($nodess) - 1][] = $node[0];
														
													// START PRE_ASS <?php
												if (
													4 < $state['precedence'][1] ||
													4 === $state['precedence'][1] &&
													$state['precedence'][0] === '<'
												) $node = false;
												else {
													$oldprecss[] = $state['precedence'];
													$state['precedence'] = ['<', 4];
													// START SEL <?php
													// <?php // START SEQ
														$backstatess[] = $state;
														$nodess[] = [];
														
														// <?php // START STR
													$node = false;
													if ( ( substr($state['input'], 0, 1) === "/" ) ) {
													
														$node = ["/"];
														$state['input'] = substr($state['input'], 1);
														$state['pos'] += 1;
														$state['line'] += 0;
														$x = strrpos("/", "\n");
														$state['col'] = $x === false ? $state['col'] + 1 : (1 - $x);
														$state['max'] = max($state['max'], $state['pos']);
													} elseif ( !$state['ignore'] ) {
														$savestate = $state;
														if ($parser::pass($context, $state, $parser)) {
															if ( ( substr($state['input'], 0, 1) === "/" ) ) {
													
																$node = ["/"];
																$state['input'] = substr($state['input'], 1);
																$state['pos'] += 1;
																$state['line'] += 0;
																$x = strrpos("/", "\n");
																$state['col'] = $x === false ? $state['col'] + 1 : (1 - $x);
																$state['max'] = max($state['max'], $state['pos']);
															} else {
																$state = $savestate;
															}
														}
													}
													//END STR
													
														if (!$node) {
															$state = array_pop($backstatess);
															array_pop($nodess);
														} else {
															$nodess[count($nodess) - 1][] = $node[0];
															
														// VAR_RULE <?php
													$node = $parser::__rule__expr($context, $state, $parser);
													
													
														if (!$node) {
															$state = array_pop($backstatess);
															array_pop($nodess);
														} else {
															$nodess[count($nodess) - 1][] = $node[0];
															array_pop($backstatess); $node = [array_pop($nodess)];
														}
														
														}
														
													// END SEQ
													
													if ($node)
														$node = [$node[0]['1']];
													// END SEL
													
													$state['precedence'] = array_pop($oldprecss);
												}
												// END PRE_ASS
												
													if (!$node) {
														$state = array_pop($backstatess);
														array_pop($nodess);
													} else {
														$nodess[count($nodess) - 1][] = $node[0];
														array_pop($backstatess); $node = [array_pop($nodess)];
													}
													
													}
													
												// END SEQ
												
												if ($node)
													$node = [// \header body -> <?php
												(function($self, $_scope){
													$_variable_this = $_scope['this'];
													return ((($_variable_this)[0]) / (($_variable_this)[1]));
												})(
														self::$expressionContext[0],
														array_merge(self::$expressionContext[1], [
															'context' => array_merge($context, ['state' => $state, 'oldState' => $oldState]),
															'this' => $node[0],
														])
													)];
												return $node;
											})($state);
											// END ACTION
											
												if (!$node) {
													// START ACTION <?php
											$node = (function ($oldState) use (&$state, &$parser, &$context) {
												// <?php // START SEQ
													$backstatess[] = $state;
													$nodess[] = [];
													
													// VAR_RULE <?php
												$node = $parser::__rule__expr($context, $state, $parser);
												
												
													if (!$node) {
														$state = array_pop($backstatess);
														array_pop($nodess);
													} else {
														$nodess[count($nodess) - 1][] = $node[0];
														
													// START PRE_ASS <?php
												if (
													3 < $state['precedence'][1] ||
													3 === $state['precedence'][1] &&
													$state['precedence'][0] === '<'
												) $node = false;
												else {
													$oldprecss[] = $state['precedence'];
													$state['precedence'] = ['<', 3];
													// START SEL <?php
													// <?php // START SEQ
														$backstatess[] = $state;
														$nodess[] = [];
														
														// <?php // START STR
													$node = false;
													if ( ( substr($state['input'], 0, 1) === "+" ) ) {
													
														$node = ["+"];
														$state['input'] = substr($state['input'], 1);
														$state['pos'] += 1;
														$state['line'] += 0;
														$x = strrpos("+", "\n");
														$state['col'] = $x === false ? $state['col'] + 1 : (1 - $x);
														$state['max'] = max($state['max'], $state['pos']);
													} elseif ( !$state['ignore'] ) {
														$savestate = $state;
														if ($parser::pass($context, $state, $parser)) {
															if ( ( substr($state['input'], 0, 1) === "+" ) ) {
													
																$node = ["+"];
																$state['input'] = substr($state['input'], 1);
																$state['pos'] += 1;
																$state['line'] += 0;
																$x = strrpos("+", "\n");
																$state['col'] = $x === false ? $state['col'] + 1 : (1 - $x);
																$state['max'] = max($state['max'], $state['pos']);
															} else {
																$state = $savestate;
															}
														}
													}
													//END STR
													
														if (!$node) {
															$state = array_pop($backstatess);
															array_pop($nodess);
														} else {
															$nodess[count($nodess) - 1][] = $node[0];
															
														// VAR_RULE <?php
													$node = $parser::__rule__expr($context, $state, $parser);
													
													
														if (!$node) {
															$state = array_pop($backstatess);
															array_pop($nodess);
														} else {
															$nodess[count($nodess) - 1][] = $node[0];
															array_pop($backstatess); $node = [array_pop($nodess)];
														}
														
														}
														
													// END SEQ
													
													if ($node)
														$node = [$node[0]['1']];
													// END SEL
													
													$state['precedence'] = array_pop($oldprecss);
												}
												// END PRE_ASS
												
													if (!$node) {
														$state = array_pop($backstatess);
														array_pop($nodess);
													} else {
														$nodess[count($nodess) - 1][] = $node[0];
														array_pop($backstatess); $node = [array_pop($nodess)];
													}
													
													}
													
												// END SEQ
												
												if ($node)
													$node = [// \header body -> <?php
												(function($self, $_scope){
													$_variable_this = $_scope['this'];
													return ((($_variable_this)[0]) + (($_variable_this)[1]));
												})(
														self::$expressionContext[0],
														array_merge(self::$expressionContext[1], [
															'context' => array_merge($context, ['state' => $state, 'oldState' => $oldState]),
															'this' => $node[0],
														])
													)];
												return $node;
											})($state);
											// END ACTION
											
												if (!$node) {
													// START ACTION <?php
											$node = (function ($oldState) use (&$state, &$parser, &$context) {
												// <?php // START SEQ
													$backstatess[] = $state;
													$nodess[] = [];
													
													// VAR_RULE <?php
												$node = $parser::__rule__expr($context, $state, $parser);
												
												
													if (!$node) {
														$state = array_pop($backstatess);
														array_pop($nodess);
													} else {
														$nodess[count($nodess) - 1][] = $node[0];
														
													// START PRE_ASS <?php
												if (
													3 < $state['precedence'][1] ||
													3 === $state['precedence'][1] &&
													$state['precedence'][0] === '<'
												) $node = false;
												else {
													$oldprecss[] = $state['precedence'];
													$state['precedence'] = ['<', 3];
													// START SEL <?php
													// <?php // START SEQ
														$backstatess[] = $state;
														$nodess[] = [];
														
														// <?php // START STR
													$node = false;
													if ( ( substr($state['input'], 0, 1) === "-" ) ) {
													
														$node = ["-"];
														$state['input'] = substr($state['input'], 1);
														$state['pos'] += 1;
														$state['line'] += 0;
														$x = strrpos("-", "\n");
														$state['col'] = $x === false ? $state['col'] + 1 : (1 - $x);
														$state['max'] = max($state['max'], $state['pos']);
													} elseif ( !$state['ignore'] ) {
														$savestate = $state;
														if ($parser::pass($context, $state, $parser)) {
															if ( ( substr($state['input'], 0, 1) === "-" ) ) {
													
																$node = ["-"];
																$state['input'] = substr($state['input'], 1);
																$state['pos'] += 1;
																$state['line'] += 0;
																$x = strrpos("-", "\n");
																$state['col'] = $x === false ? $state['col'] + 1 : (1 - $x);
																$state['max'] = max($state['max'], $state['pos']);
															} else {
																$state = $savestate;
															}
														}
													}
													//END STR
													
														if (!$node) {
															$state = array_pop($backstatess);
															array_pop($nodess);
														} else {
															$nodess[count($nodess) - 1][] = $node[0];
															
														// VAR_RULE <?php
													$node = $parser::__rule__expr($context, $state, $parser);
													
													
														if (!$node) {
															$state = array_pop($backstatess);
															array_pop($nodess);
														} else {
															$nodess[count($nodess) - 1][] = $node[0];
															array_pop($backstatess); $node = [array_pop($nodess)];
														}
														
														}
														
													// END SEQ
													
													if ($node)
														$node = [$node[0]['1']];
													// END SEL
													
													$state['precedence'] = array_pop($oldprecss);
												}
												// END PRE_ASS
												
													if (!$node) {
														$state = array_pop($backstatess);
														array_pop($nodess);
													} else {
														$nodess[count($nodess) - 1][] = $node[0];
														array_pop($backstatess); $node = [array_pop($nodess)];
													}
													
													}
													
												// END SEQ
												
												if ($node)
													$node = [// \header body -> <?php
												(function($self, $_scope){
													$_variable_this = $_scope['this'];
													return ((($_variable_this)[0]) - (($_variable_this)[1]));
												})(
														self::$expressionContext[0],
														array_merge(self::$expressionContext[1], [
															'context' => array_merge($context, ['state' => $state, 'oldState' => $oldState]),
															'this' => $node[0],
														])
													)];
												return $node;
											})($state);
											// END ACTION
											
												if (!$node) {
													$node = false;
												}
												}
												}
												}
												}
											// END ALT_GRO GROW
											if ( $state['pos'] <= $pos || !$node ) {
												$mem = $context['memo'][$growMemoKey];
												if ( $mem[1] ) {
													$state['input'] = $mem[0]['input'];
													$state['pos'] = $mem[0]['pos'];
													$state['precedence'] = $mem[0]['precedence'];
												}
												$node = $mem[1];
												break;
											}
										} while(true);
										// END ALT_GRO LOOP
									// END ALT_GRO
									
									// END MEMO_BODY
									$context['memo'][$growMemoKey] = [$state, $node];
								}
								// END MEMO
								
								
								return $node;
								// END RULE
								
								;}
							static function __rule__const ($context, &$state, $parser) {// <?php // START RULE
								// START MEMO <?php
								$ruleMemoKey = "{$state['pos']}{$state['precedence'][0]}{$state['precedence'][1]}\$rule:const.\${$parser->parserId}";
								if ( !empty($context['memo'][$ruleMemoKey]) ) {
									$mem = $context['memo'][$ruleMemoKey];
									if ( $mem[1] ) {
										$state['input'] = $mem[0]['input'];
										$state['pos'] = $mem[0]['pos'];
										$state['line'] = $mem[0]['line'];
										$state['col'] = $mem[0]['col'];
										$state['precedence'] = $mem[0]['precedence'];
									}
									$node = $mem[1];
								} else {
									// START MEMO_BODY
									
									$nodes = [];
									$node = false;
									// START ACTION <?php
									$node = (function ($oldState) use (&$state, &$parser, &$context) {
										// <?php // START STR
										$node = false;
										if ( ( substr($state['input'], 0, 1) === "e" ) ) {
										
											$node = ["e"];
											$state['input'] = substr($state['input'], 1);
											$state['pos'] += 1;
											$state['line'] += 0;
											$x = strrpos("e", "\n");
											$state['col'] = $x === false ? $state['col'] + 1 : (1 - $x);
											$state['max'] = max($state['max'], $state['pos']);
										} elseif ( !$state['ignore'] ) {
											$savestate = $state;
											if ($parser::pass($context, $state, $parser)) {
												if ( ( substr($state['input'], 0, 1) === "e" ) ) {
										
													$node = ["e"];
													$state['input'] = substr($state['input'], 1);
													$state['pos'] += 1;
													$state['line'] += 0;
													$x = strrpos("e", "\n");
													$state['col'] = $x === false ? $state['col'] + 1 : (1 - $x);
													$state['max'] = max($state['max'], $state['pos']);
												} else {
													$state = $savestate;
												}
											}
										}
										//END STR
										
											if (!$node) {
												// <?php // START STR
										$node = false;
										if ( ( substr($state['input'], 0, 2) === "pi" ) ) {
										
											$node = ["pi"];
											$state['input'] = substr($state['input'], 2);
											$state['pos'] += 2;
											$state['line'] += 0;
											$x = strrpos("pi", "\n");
											$state['col'] = $x === false ? $state['col'] + 2 : (2 - $x);
											$state['max'] = max($state['max'], $state['pos']);
										} elseif ( !$state['ignore'] ) {
											$savestate = $state;
											if ($parser::pass($context, $state, $parser)) {
												if ( ( substr($state['input'], 0, 2) === "pi" ) ) {
										
													$node = ["pi"];
													$state['input'] = substr($state['input'], 2);
													$state['pos'] += 2;
													$state['line'] += 0;
													$x = strrpos("pi", "\n");
													$state['col'] = $x === false ? $state['col'] + 2 : (2 - $x);
													$state['max'] = max($state['max'], $state['pos']);
												} else {
													$state = $savestate;
												}
											}
										}
										//END STR
										
											if (!$node) {
												$node = false;
											}
											}
										if ($node)
											$node = [// \header body -> <?php
										(function($self, $_scope){
											$_variable_context = $_scope['context'];
											$_variable_this = $_scope['this'];
											return (((($_variable_context)["data"]))[$_variable_this]);
										})(
												self::$expressionContext[0],
												array_merge(self::$expressionContext[1], [
													'context' => array_merge($context, ['state' => $state, 'oldState' => $oldState]),
													'this' => $node[0],
												])
											)];
										return $node;
									})($state);
									// END ACTION
									
									
									// END MEMO_BODY
									$context['memo'][$ruleMemoKey] = [$state, $node];
								}
								// END MEMO
								
								return $node;
								// END RULE
								
								;}
							static function __rule__int ($context, &$state, $parser) {// <?php // START RULE
								// START MEMO <?php
								$ruleMemoKey = "{$state['pos']}{$state['precedence'][0]}{$state['precedence'][1]}\$rule:int.\${$parser->parserId}";
								if ( !empty($context['memo'][$ruleMemoKey]) ) {
									$mem = $context['memo'][$ruleMemoKey];
									if ( $mem[1] ) {
										$state['input'] = $mem[0]['input'];
										$state['pos'] = $mem[0]['pos'];
										$state['line'] = $mem[0]['line'];
										$state['col'] = $mem[0]['col'];
										$state['precedence'] = $mem[0]['precedence'];
									}
									$node = $mem[1];
								} else {
									// START MEMO_BODY
									
									$nodes = [];
									$node = false;
									// START ACTION <?php
									$node = (function ($oldState) use (&$parser, &$state, &$context) {
										// START SEL <?php
										// <?php // START REG
										$node = false;
										if ( preg_match("/^\\d+/", $state['input'], $m) ) {
											$len = strlen($m[0]);
											$node = [$m];
											$state['input'] = substr($state['input'], $len);
											$state['pos'] += $len;
											$state['line'] += substr_count($m[0], "\n");
											$x = strrpos($m[0], "\n");
											$state['col'] = $x === false ? $state['col'] + $len : ($len - $x);
											$state['max'] = max($state['max'], $state['pos']);
										} elseif ( !$state['ignore'] ) {
											$savestate = $state;
											if ($parser::pass($context, $state, $parser)) {
												if ( preg_match("/^\\d+/", $state['input'], $m) ) {
													$len = strlen($m[0]);
													$node = [$m];
													$state['input'] = substr($state['input'], $len);
													$state['pos'] += $len;
													$state['line'] += substr_count($m[0], "\n");
													$x = strrpos($m[0], "\n");
													$state['col'] = $x === false ? $state['col'] + $len : ($len - $x);
													$state['max'] = max($state['max'], $state['pos']);
												} else {
													$state = $savestate;
												}
											}
										}
										//END REG
										
										if ($node)
											$node = [$node[0]['0']];
										// END SEL
										
										if ($node)
											$node = [// \header body -> <?php
										(function($self, $_scope){
											$_variable_implementation = $_scope['implementation'];
											$_variable_this = $_scope['this'];
											return $_variable_implementation(
												1
											)(
												"intval"
											)(
												$_variable_this
											);
										})(
												self::$expressionContext[0],
												array_merge(self::$expressionContext[1], [
													'context' => array_merge($context, ['state' => $state, 'oldState' => $oldState]),
													'this' => $node[0],
												])
											)];
										return $node;
									})($state);
									// END ACTION
									
									
									// END MEMO_BODY
									$context['memo'][$ruleMemoKey] = [$state, $node];
								}
								// END MEMO
								
								return $node;
								// END RULE
								
								;}
							
						
						}));
						// END SCO_BOT
						
					})($expressionContext);
					
					return $node;
				})([$self, [
					'implementation' => &$_variable_implementation
				]]));
				$_variable_results = [
					$_variable_parser(
						"S"
					)(
						[
							$_variable_constants,
							"1 + 2 * 5 * 3 ^ 4 ^ 2 / 2 / 5 - 1"
						]
					),
					$_variable_parser(
						"S"
					)(
						[
							$_variable_constants,
							"e ^ pi"
						]
					),
					$_variable_parser(
						"S"
					)(
						[
							$_variable_constants,
							"pi ^ e"
						]
					)
				];
				return $f(
					$_variable_constants,
					$_variable_parser,
					$_variable_results
				);
			})(function (
				$_variable_constants,
				$_variable_parser,
				$_variable_results
			) use (
				&$_variable_die
			) {
				return $_variable_die(
				$_variable_results
			);
			}))
			;
		}

		static private $singletons = [];
		static $structure, $nil;
		public $nspace = [];

		function __construct($_scope) {
			self::$structure = self::getStructure('INSTANCE', 0, 'INSTANCE', []);
			self::$nil = self::getStructure('Data', 0, '()', []);
			$this->nspace = $this->lambda($this->scope($_scope));
			set_error_handler(function ( $errno, $errstr, $errfile, $errline ) {
				throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
			});
		}

		static function funConv($argc, $fun, $args = []) {
			if (!$argc)
				return $fun(...$args);
			return function ($x) use($argc, $args, $fun) {
				return self::funConv($argc - 1, $fun, array_merge($args, [$x]));
			};
		}

		function scope ($scope = []) {
			return $scope = [
				'reimp' => function ($f) {return function (...$args) use($f) { return array_reduce($args, function($f, $a) {return $f($a);}, $f); };},
				'implementation' => self::funConv(2, [$this, 'funConv']),
				'toString' => self::funConv(1, [$this, 'toString']),
				'include' => function ($e) { return include($e); },
				'eval' => function ($code) { return eval($code); },
				'die' => function ($e) { die(substr(is_string($e) ? $e : $this->toString($e), 0, 10000000)); },
				'echo' => function ($e) { echo substr(is_string($e) ? $e : $this->toString($e), 0, 10000000), "\n"; ob_flush(); return true; },
				'preg_match' => self::funConv(2, function($patt, $str) {preg_match($patt, $str, $m); return $m;}),
				'throw' => function($e) {throw new Exception($e);},
			];
		}

		static function just($a) {
			return self::getStructure('Data', 0, ':>', [$a, self::$nil]);
		}

		static function cons($a, $b) {
			return self::getStructure('Data', 0, ':>', [$a, $b]);
		}

		static function getStructure($kind, $arity, $name, $args) {

			if ( !$args ) {
				$k = "$arity:$kind:$name";
				if (!empty(self::$singletons[$k]))
					return self::$singletons[$k];
			}

			$a = (new class ($kind, $arity, $name, $args) {

				public $kind = "", $arity, $name, $args;

				public function __construct ($kind, $arity, $name, $args = []) {
					$this->kind = $kind;
					$this->arity = $arity;
					$this->name = $name;
					$this->args = $args;
				}

				function __invoke ( $arg ) {
					if ( $this->arity )
						return new static($this->kind, $this->arity -1, $this->name, array_merge($this->args, [$arg]));
					throw new \Exception("Can't apply {$this->kind} constructor \"{$this->name}\" anymore, arity exceeded !");
				}

			});

			if ( !$args )
				self::$singletons[$k] = $a;
			return $a;

		}

		function toStringList ( $data, $ind, $visited ) {
			if ( array_reduce($visited, function($a, $v) use($data){return $a || $v ===$data;}, false) )
				return ["<RECURSION>"];
			$visited[] = $data;

			switch(true) {
				case $data instanceof self::$structure && $data->kind === 'Data' && $data->name === ":>":
					return array_merge([$this->toString($data->args[0], $ind, $visited)], $this->toStringList($data->args[1], $ind, $visited));
				case $data instanceof self::$structure && $data->kind === 'Data' && $data->name === "()":
					return [];
			}
		}

		function toString ( $data, $ind = "", $visited = [] ) {

			if ( array_reduce($visited, function($a, $v) use($data){return $a || $v ===$data;}, false) )
				return "<RECURSION>";
			$visited[] = $data;

			switch ( true ) {

				case is_null($data):
					return "null";

				case is_bool($data):
					return $data ? "true" : "false";

				case is_numeric($data):
					return "$data";

				case is_string($data):
					return '"' . str_replace(['\\', '"'], ['\\\\', '\\"'], $data) . '"';

				case is_array($data) && !$data:
					return "[]";

				case is_array($data) && is_string(array_keys($data)[0]):
					return "{" . implode("", array_map(
						function ( $k, $v ) use ($ind, $visited, $data) { return "\n\t$ind$k: " . $this->toString($v, "$ind\t", $visited) . ","; },
						array_keys($data),
						$data
					)) . "\n$ind}";

				case is_array($data):
					return "{" . implode("", array_map(
						function ($v) use ($ind, $visited, $data) {
							return "\n\t$ind" . $this->toString($v, "$ind\t", $visited) . ",";
						},
						$data
					)) . "\n$ind}";

				case $data instanceof self::$structure && $data->kind === 'Data' && $data->name === ":>":
					array_pop($visited);
					$rrr = $this->toStringList($data, "$ind\t", $visited);
					$rrt = implode(", ",  $rrr);
					if ( strlen($rrt) > 110 )
						return "(\n$ind\t" . implode(",\n$ind\t", $rrr) . "\n$ind)";
					return "(" . $rrt . ")";

				case $data instanceof self::$structure && $data->kind === 'Data' && $data->name === "()":
					return '()';

				case $data instanceof self::$structure && $data->kind === 'Data':
					if ( !$data->args )
						return "{$data->name}";
					return "({$data->name} " . implode(" ",
						array_map(function ($e) use ($ind, $visited, $data) { return $this->toString($e, "$ind", $visited); },
						$data->args
					)) . ")";

				case $data instanceof self::$structure && $data->kind === 'Type':
					return "(#{$data->name}" . (!$data->args ? '' : ' ' . implode(' ', array_map(
						function ($e) use($ind, $visited, $data) { return $this->toString($e, $ind, $visited); },
						$data->args
					))) . ')';

				 case $data instanceof \Closure:
					$clo = new ReflectionFunction($data);
					$params = implode(", ", array_map(function($a) {return $a->name;}, $clo->getParameters()));
					$closeds = implode(', ', array_keys($clo->getStaticVariables()));
					return "(# \\$params -> <CLOSURE>, [$closeds])";

				default: return sprintf('#%1$s%2$s', get_class($data), self::toString((array) $data, "$ind", $visited));

			}
		}

	});

};
})()();